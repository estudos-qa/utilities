import os
import urllib.request
import zipfile
import subprocess
import sys

def download_instant_client(url, dest_folder):
    try:
        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder)
        file_name = url.split('/')[-1]
        dest_path = os.path.join(dest_folder, file_name)

        print(f"Baixando {file_name} de {url} para {dest_path}...")
        urllib.request.urlretrieve(url, dest_path)
        print("Download concluído.")

        return dest_path
    except Exception as e:
        print(f"Erro ao baixar o Instant Client: {e}")
        sys.exit(1)

def extract_zip(file_path, extract_to):
    try:
        print(f"Extraindo {file_path} para {extract_to}...")
        with zipfile.ZipFile(file_path, 'r') as zip_ref:
            zip_ref.extractall(extract_to)
        print("Extração concluída.")
    except Exception as e:
        print(f"Erro ao extrair o Instant Client: {e}")
        sys.exit(1)

def set_environment_variable(variable, value):
    try:
        print(f"Configurando variável de ambiente {variable}...")
        subprocess.run(['setx', variable, value], shell=True, check=True)
        print(f"Variável de ambiente {variable} configurada para {value}.")
    except subprocess.CalledProcessError as e:
        print(f"Erro ao configurar a variável de ambiente {variable}: {e}")
        sys.exit(1)

def main():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    oracle_url = "https://download.oracle.com/otn_software/windows/instantclient/instantclient-basiclite-windows.x64-21.6.0.0.0.zip"
    dest_folder = os.path.join(current_dir, "oracle_instantclient")
    extract_to = dest_folder

    # Baixar e extrair o Instant Client
    zip_path = download_instant_client(oracle_url, dest_folder)
    extract_zip(zip_path, extract_to)

    # Configurar variáveis de ambiente
    set_environment_variable("ORACLE_HOME", extract_to)
    set_environment_variable("TNS_ADMIN", os.path.join(extract_to, "network", "admin"))
    set_environment_variable("PATH", f"{extract_to};%PATH%")

if __name__ == "__main__":
    main()
